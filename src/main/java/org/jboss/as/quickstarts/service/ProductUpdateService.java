package org.jboss.as.quickstarts.service;

import org.jboss.as.quickstarts.entity.Product;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface ProductUpdateService {

    @GET("/api/products")
    Call<List<Product>> getProducts();

}
