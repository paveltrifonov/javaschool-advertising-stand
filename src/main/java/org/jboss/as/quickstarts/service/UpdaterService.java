package org.jboss.as.quickstarts.service;

import org.jboss.as.quickstarts.dao.ProductDAO;
import org.jboss.as.quickstarts.entity.Product;
import retrofit2.Call;
import retrofit2.Response;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class UpdaterService {

    private static final Logger LOGGER = Logger.getLogger(UpdaterService.class.toString());

    @Inject
    private ProductUpdateService productUpdateService;

    @Inject
    private ProductDAO productDAO;

    public void updateProducts() {
        productDAO.deleteAll();
        List<Product> updatedProducts;
        try {
            Call<List<Product>> productCall = productUpdateService.getProducts();
            Response<List<Product>> response = productCall.execute();
            updatedProducts = response.body();
        } catch (IOException e) {
            LOGGER.info("Error");
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        for (Product p : updatedProducts) {
            productDAO.save(p);
        }
    }

}
