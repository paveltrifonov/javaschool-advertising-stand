package org.jboss.as.quickstarts.bean;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("counterView")
@RequestScoped
public class CounterView implements Serializable {

    private int number;

    public void increment() {
        number++;

    }
}