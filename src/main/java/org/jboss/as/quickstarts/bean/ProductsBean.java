package org.jboss.as.quickstarts.bean;

import org.jboss.as.quickstarts.dao.ProductDAO;
import org.jboss.as.quickstarts.entity.Product;

import javax.ejb.EJB;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;

@Named(value = "productsBean")
@RequestScoped
public class ProductsBean {

    @EJB
    private ProductDAO productDAO;

    private List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        products = productDAO.findAll();
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
