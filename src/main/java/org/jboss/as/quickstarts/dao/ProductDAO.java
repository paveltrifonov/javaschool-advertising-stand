package org.jboss.as.quickstarts.dao;

import org.jboss.as.quickstarts.entity.Product;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ProductDAO {

    @PersistenceContext(unitName = "Banner")
    private EntityManager em;

    public void save(Product product){
        em.persist(product);
    }

    public Product getProduct(String code){

      return (Product) em.createQuery("select p from Product p where p.code = :code").setParameter("code", code).getSingleResult();
    }

    public List<Product> findAll() {
        return em.createQuery("select p from Product p").getResultList();
    }

    public void deleteAll() {
        em.createNativeQuery("delete from products").executeUpdate();
    }
}
